
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyB23sq1Ql1GBwTlH6v_BfTTQDAS-8D2VdE',
    authDomain: 'zpw-mean.firebaseapp.com',
    databaseURL: 'https://zpw-mean.firebaseio.com',
    projectId: 'zpw-mean',
    storageBucket: 'zpw-mean.appspot.com',
    messagingSenderId: '978196834370',
    appId: '1:978196834370:web:af4cfd940b8f4f5fa3a5db'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
