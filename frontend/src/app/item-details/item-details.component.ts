import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ItemService } from '../services/item.service';
import { AuthService } from '../services/auth.service';
import { Item } from '../models/item';

@Component({
  selector: 'app-item-details',
  templateUrl: './item-details.component.html',
  styleUrls: ['./item-details.component.css']
})
export class ItemDetailsComponent implements OnInit{
  item: Item;

  constructor(private itemService: ItemService, private route: ActivatedRoute, private auth: AuthService) {}

  ngOnInit() {
    this.getItem();
  }

  isAdmin(): boolean {
    return this.auth.isAdmin();
  }

  getItem(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.itemService.getItem(id)
      .subscribe(item => this.item = item);
  }
  updateItem(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.itemService.updateItem(id, this.item);
  }

}
