import { Injectable } from '@angular/core';
import { Item } from '../models/item';
import {ItemService} from './item.service';
import {AuthService} from 'src/app/services/auth.service';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {
  shoppingCart = [];
  constructor(private itemService: ItemService, private auth: AuthService, private router: Router) {
    //Load from localStorage
    const shoppingCartString = localStorage.getItem('shoppingCart');
    if (shoppingCartString) {
      const shoppingCart = JSON.parse(shoppingCartString);
      const uniqueIds = [...new Set(shoppingCart.map((item: Item) => item.id))];
      uniqueIds.forEach(id => {
          const count = shoppingCart.filter((item: Item) => item.id === id).length;
          this.itemService.getItem(id).subscribe(item => this.shoppingCart.push(...Array(count).fill(item)));
      });
    }
  }

  getItemsInShoppingCart(): Array<Item> {
    return this.shoppingCart;
  }

  addItemToShoppingCart(item: Item) {
    if (this.existsInShoppingCart(item)) { // Overide if exits, to make sure it's the same element
      item = this.shoppingCart.find(i => i.id === item.id);
    }
    this.shoppingCart.push(item);
    localStorage.setItem('shoppingCart', JSON.stringify(this.shoppingCart));
  }

  removeItemFromShoppingCart(item: Item) {
    const index = this.shoppingCart.indexOf(item);
    if (index > -1) {
      this.shoppingCart.splice(index, 1);
    }
    localStorage.setItem('shoppingCart', JSON.stringify(this.shoppingCart));
  }

  existsInShoppingCart(item: Item): boolean {
    return !(this.shoppingCart.find(i => i.id === item.id) === undefined);
  }

  countItemInShoppingCart(item: Item) {
    return this.shoppingCart.filter(i => i.id === item.id).length;
  }

  countItemsInShoppingCart(): number {
   return this.shoppingCart.length;
  }

  getUniqueItemsInShoppingCart(): Set<Item> {
    return new Set(this.shoppingCart);
  }

  orderItems() {
    if (!this.auth.getUser()) { this.router.navigate(['/signin']); return; }
    this.itemService.orderItems(this.shoppingCart); // .subscribe(item => console.log('Bought'));
    this.shoppingCart = [];
    localStorage.removeItem('shoppingCart');
  }
}
