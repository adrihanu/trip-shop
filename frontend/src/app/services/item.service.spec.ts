import { TestBed } from '@angular/core/testing';

import { ItemService } from './item.service';
import {ReactiveFormsModule} from '@angular/forms';
import {FormsModule} from '@angular/forms';
import { environment } from 'src/environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { HttpClientModule } from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';
import {Item} from 'src/app/models/item';


describe('ItemService', () => {
  beforeEach(() => TestBed.configureTestingModule({
        imports: [ ReactiveFormsModule, FormsModule, AngularFireAuthModule, AngularFireModule.initializeApp(environment.firebaseConfig), AngularFirestoreModule, HttpClientModule, RouterTestingModule],
  }));

  it('should be created', () => {
    const service: ItemService = TestBed.get(ItemService);
    expect(service).toBeTruthy();
  });

  it('should get items', (done) => {
    const service: ItemService = TestBed.get(ItemService);
    const items = service.getItems().subscribe( (result) => {
     let count = result.length;
     expect(count).toBeGreaterThan(0);
     done();
    });

   

  });
});
