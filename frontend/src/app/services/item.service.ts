import { Injectable} from '@angular/core';
import { Observable, of } from 'rxjs';
import { Item } from '../models/item';
import { Comment } from '../models/comment';
import { Order } from '../models/order';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AuthService } from './auth.service';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ItemService {
  private itemsCollection: AngularFirestoreCollection<Item>;
  private usersOrdersCollection: AngularFirestoreCollection<Order>;
  private usersCommentsCollection: AngularFirestoreCollection<Comment>;

  items: Observable<Item[]>;
  private backendUrl = 'api/items';  // URL to web api
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };


  constructor( private http: HttpClient, private db: AngularFirestore, private auth: AuthService, private router: Router) {
    this.itemsCollection = this.db.collection<Item>('items');
    this.usersOrdersCollection = this.db.collection<Order>('usersOrders');
    this.usersCommentsCollection = this.db.collection<Comment>('usersComments');
  }


  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error); // log to console instead
      alert(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }


  /**
   * Fetch items from database.
   */
  getItems(): Observable<Item[]> {
    // Firebase
    this.items = this.itemsCollection.valueChanges({idField: 'id'});
    return this.items;
    // API
    return this.http.get<Item[]>(this.backendUrl)
                     .pipe(tap(_ => console.log('Fetched items from API')),
                     catchError(this.handleError<Item[]>('getItemsFromAPI', [])));
  }

  /**
   * Fetch item from database.
   */
  getItem(id: any): Observable<Item> {
     // Firebase
    const item = this.db.doc<Item>(`/items/${id}`).snapshotChanges().pipe(map((action: any) =>  {
      const object = action.payload.data();
      object.id = action.payload.id;
      return object;
    }));
    return item;
    // API
    const url = `${this.backendUrl}/${id}`;
    return this.http.get<Item>(url)
                    .pipe(tap(_ => console.log('fetched item')),
                          catchError(this.handleError<Item>('getItem')));
  }

  /**
   * Add item to database.
   */
  addItem(item: Item): Observable<Item> {
    // Firebase
    this.itemsCollection.add(item);
    return of(item);
    // API
    return this.http.post<Item>(this.backendUrl, item, this.httpOptions)
                    .pipe(tap(_ => console.log('added item')),
                          catchError(this.handleError<Item>('addItem')));
  }

  /**
   * Fetch item from database.
   */
  updateItem(id: any, data: any, confirm = true): any {
    // Firebase
   this.db.doc<Item>(`/items/${id}`).update(data).then(res => { if (confirm) {alert('Updated item!'); } });
  }

  /**
   * Delete item from database.
   */
  removeItem(item: Item): Observable<Item> {
    // Firebase
    this.db.doc<Item>(`/items/${item.id}`).delete();
    return of(item);
    // API
    const url = `${this.backendUrl}/${item.id}`;
    return this.http.delete<Item>(url, this.httpOptions)
                    .pipe(tap(_ => console.log('removed item')),
                          catchError(this.handleError<Item>('removeItem')));
  }

  /**
   * Order items
   */
  orderItems(items: Item[]) {
    if (!this.auth.getUser()) { this.router.navigate(['/signin']);
    } else {

      // Update limits
      const uniqueIds = [...new Set(items.map((item: Item) => item.id))];
      uniqueIds.forEach(id => {
          const exampleItem = items.find((item: Item) => item.id === id);
          const count = items.filter((item: Item) => item.id === id).length;
          this.updateItem(id, {limit: exampleItem.limit - count}, false);
      });

      // Save orders
      items.forEach(item => {
         const order = {itemId: item.id, userId: this.auth.getUser().uid};
         this.usersOrdersCollection.add(order);
      });
    }
  }


  /**
   * Get ordered items
   */
  getOrderedItems() {
    const userId = this.auth.getUserId();
    return this.db.collection<Order>('usersOrders',  ref => ref.where('userId', '==', userId )).valueChanges();
  }

  /**
   * Get comments for order
   */
  getComments(itemId: string) {
    return this.db.collection<Comment>('usersComments',  ref => ref.where('itemId', '==', itemId )).valueChanges();
  }

  /**
   * Add item to database.
   */
  addComment(comment: Comment) {
    // Firebase
    this.usersCommentsCollection.add(comment);
  }
}
