import { Component, OnInit, Input } from '@angular/core';
import {ItemService} from 'src/app/services/item.service';
import {Comment} from 'src/app/models/comment';
import {Item} from 'src/app/models/item';
import {AuthService} from 'src/app/services/auth.service';
import {Order} from 'src/app/models/order';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {
  comment: string;
  comments: Comment[];
  canComment: boolean;
  @Input() item: Item;
  constructor(private auth: AuthService, private itemService: ItemService) { }

  ngOnInit() {

   this.itemService.getComments(this.item.id).subscribe((comments: Comment[]) => {
     this.comments = comments;
   });

   this.itemService.getOrderedItems().subscribe((orders: any) => {
      orders = orders.filter((order: Order) => order.itemId === this.item.id);
      const usersIds = [...new Set(orders.map((order: any) => order.userId))];
      this.canComment = usersIds.includes(this.auth.getUserId());
   });
  }

  addComment() {
    if (!this.comment) { return; }
    const comment = {text: this.comment, itemId: this.item.id} as Comment;
    this.itemService.addComment(comment);
    this.comment = '';
  }
}
