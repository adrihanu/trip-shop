import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentComponent } from './comment.component';
import {FormsModule} from '@angular/forms';

import { environment } from 'src/environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { HttpClientModule } from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';
import {Item} from 'src/app/models/item';
import {Comment} from 'src/app/models/comment';
import {ItemService} from 'src/app/services/item.service';
import { Observable, of } from 'rxjs';

class MockItemService {
  comments = [];

  getComments(id: number): Observable<string[]>{
    return of(['ss']);
  }
  getOrderedItems(id: number): Observable<string[]>{
    return of(['ss']);
  }

  addComment(comment: String) {
    return this.comments.push(comment);
  }

  countComments(): number{
    return this.comments.length;
  }
}


describe('CommentComponent', () => {
  let component: CommentComponent;
  let itemService: MockItemService;
  let fixture: ComponentFixture<CommentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule, AngularFireAuthModule, AngularFireModule.initializeApp(environment.firebaseConfig), AngularFirestoreModule, HttpClientModule, RouterTestingModule],
      providers: [
        {provide: ItemService, useClass: MockItemService}
      ],
      declarations: [ CommentComponent ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentComponent);
    component = fixture.componentInstance;

    itemService = TestBed.get(ItemService);

    component.item = {id: '22323'} as Item;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add comment', () => {
      component.comment = 'New comment';
      component.addComment();
      var count = itemService.countComments();
      expect(count).toEqual(1);
  });

  it('should not add empty comment', () => {
      component.comment = '';
      component.addComment();
      var count = itemService.countComments();
      expect(count).toEqual(0);
  });
});
