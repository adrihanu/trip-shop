import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ItemsComponent } from './items/items.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { ContactComponent } from './contact/contact.component';
import { ItemComponent } from './item/item.component';
import { NewItemComponent } from './new-item/new-item.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { HomeComponent } from './home/home.component';
import { ShoppingCartItemComponent } from './shopping-cart-item/shopping-cart-item.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ItemDestinationPipe } from './pipes/item-destination.pipe';
import { ItemStartDatePipe } from './pipes/item-start-date.pipe';
import { ItemEndDatePipe } from './pipes/item-end-date.pipe';
import { ItemMinPricePipe } from './pipes/item-min-price.pipe';
import { ItemMaxPricePipe } from './pipes/item-max-price.pipe';
import { ItemMinRatingPipe } from './pipes/item-min-rating.pipe';
import { ItemDetailsComponent } from './item-details/item-details.component';

import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './services/in-memory-data.service';

import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../environments/environment';

import { AngularFirestoreModule } from '@angular/fire/firestore';
import { OrderComponent } from './order/order.component';
import { OrderedItemComponent } from './ordered-item/ordered-item.component';
import { CommentComponent } from './comment/comment.component';

@NgModule({
  declarations: [
    AppComponent,
    ItemsComponent,
    NavbarComponent,
    FooterComponent,
    ContactComponent,
    ItemComponent,
    NewItemComponent,
    ShoppingCartComponent,
    HomeComponent,
    ShoppingCartItemComponent,
    SignInComponent,
    SignUpComponent,
    ItemDestinationPipe,
    ItemStartDatePipe,
    ItemEndDatePipe,
    ItemMinPricePipe,
    ItemMaxPricePipe,
    ItemMinRatingPipe,
    ItemDetailsComponent,
    OrderComponent,
    OrderedItemComponent,
    CommentComponent
  ],
  imports: [
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    ),
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule, // do obsługi autentykacji
    AngularFirestoreModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
