import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemsComponent } from './items.component';

import {FormsModule} from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';

import { environment } from 'src/environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { HttpClientModule } from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';
import {Item} from 'src/app/models/item';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { ItemMinRatingPipe } from 'src/app/pipes/item-min-rating.pipe';
import { ItemMinPricePipe } from 'src/app/pipes/item-min-price.pipe';
import { ItemMaxPricePipe } from 'src/app/pipes/item-max-price.pipe';
import { ItemStartDatePipe } from 'src/app/pipes/item-start-date.pipe';
import { ItemEndDatePipe } from 'src/app/pipes/item-end-date.pipe';
import { ItemDestinationPipe } from 'src/app/pipes/item-destination.pipe';
import { Observable, of } from 'rxjs';
import {ItemService} from 'src/app/services/item.service';

class MockItemService {
  items = [];

  getItems(): Observable<string[]>{
    return of(['ss']);
  }

  addItem(item: Item) {
    this.items.push(item);
    return of(['ss']);
  }

  removeItem(id: number) {
    this.items = [];

    return of(['ss']);
  }

  countItems(): number{
    return this.items.length;
  }
}

describe('ItemsComponent', () => {
  let component: ItemsComponent;
  let fixture: ComponentFixture<ItemsComponent>;
  let itemService: MockItemService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ ReactiveFormsModule, FormsModule, AngularFireAuthModule, AngularFireModule.initializeApp(environment.firebaseConfig), AngularFirestoreModule, HttpClientModule, RouterTestingModule],
      declarations: [ ItemsComponent, ItemMinRatingPipe, ItemMinPricePipe, ItemMaxPricePipe, ItemDestinationPipe, ItemStartDatePipe, ItemEndDatePipe ],
      providers: [
        {provide: ItemService, useClass: MockItemService}
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemsComponent);
    component = fixture.componentInstance;
    itemService = TestBed.get(ItemService);

    let item1 = {id: '22323'} as Item;
    let item2 = {id: '22323'} as Item;
    component.items = [item1, item2];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add item', () => {
   let item = {name: 'test', price: 11, startDate: new Date('22/22/2222'), endDate: new Date('11/11/3111'), rating: 0, destination: "x", limit: 2, id: 'ss', description: 'xx', imageSrc:''   } as Item;
      component.addItem(item);
      var count = itemService.countItems();
      expect(count).toEqual(1);
  });

  it('should not add item with empty name', () => {
      let item = {name: '', price: 11, startDate: new Date('22/22/2222'), endDate: new Date('11/11/3111'), rating: 0, destination: "x", limit: 2, id: 'ss', description: 'xx', imageSrc:''   } as Item;
      component.addItem(item);
      var count = itemService.countItems();
      expect(count).toEqual(0);
  });

  it('should not add item with negative price', () => {
      let item = {name: 'aaa', price: -11, startDate: new Date('22/22/2222'), endDate: new Date('11/11/3111'), rating: 0, destination: "x", limit: 2, id: 'ss', description: 'xx', imageSrc:''   } as Item;
      component.addItem(item);
      var count = itemService.countItems();
      expect(count).toEqual(0);
  });

  it('should not add item without limit', () => {
      let item = {name: 'aaa', price: 11, startDate: new Date('22/22/2222'), endDate: new Date('11/11/3111'), rating: 0, destination: "x", id: 'ss', description: 'xx', imageSrc:''   } as Item;
      component.addItem(item);
      var count = itemService.countItems();
      expect(count).toEqual(0);
  });

  it('should not add item without startDate', () => {
   let item = {name: 'aaa', price: 11, endDate: new Date('11/11/3111'), rating: 0, destination: "x", id: 'ss', limit: 21, description: 'xx', imageSrc:''   } as Item;
      component.addItem(item);
      var count = itemService.countItems();
      expect(count).toEqual(0);
  });


  it('should remove item', () => {
      let item = {name: 'aaa', price: 11, endDate: new Date('11/11/3111'), rating: 0, destination: "x", id: 'ss', limit: 21, description: 'xx', imageSrc:''   } as Item;
      itemService.addItem(item);

      component.removeItem(item);
      var count = itemService.countItems();
      expect(count).toEqual(0);
  });
});
