export interface Comment {
    itemId: string;
    text: string;
}
