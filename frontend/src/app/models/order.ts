export interface Order {
    userId: string;
    itemId: string;
}
