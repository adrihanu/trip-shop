export class Item {
  id: string;
  name: string;
  destination: string;
  startDate: Date;
  endDate: Date;
  rating: number;
  price: number;
  limit: number;
  description: string;
  imageSrc: string;
}
