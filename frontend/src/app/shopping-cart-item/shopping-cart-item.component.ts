import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { ShoppingCartService } from '../services/shopping-cart.service';
import { Item } from '../models/item';

@Component({
  selector: 'app-shopping-cart-item',
  templateUrl: './shopping-cart-item.component.html',
  styleUrls: ['./shopping-cart-item.component.css']
})
export class ShoppingCartItemComponent implements OnInit {

  @Input() item: Item;
  @Output() removeItemEmitter = new EventEmitter<Item>();

  constructor(private shoppingCartService: ShoppingCartService) {}


  ngOnInit() {
  }

  addItemToShoppingCart(): void {
      this.shoppingCartService.addItemToShoppingCart(this.item);
  }
  removeItemFromShoppingCart(): void {
      this.shoppingCartService.removeItemFromShoppingCart(this.item);
  }

  howManyLeft(): number {
    return this.item.limit - (this.shoppingCartService.countItemInShoppingCart(this.item));
  }
}
