import { Component, OnInit } from '@angular/core';
import { ShoppingCartService } from '../services/shopping-cart.service';
import { Item } from '../models/item';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {

  constructor(private shoppingCartService: ShoppingCartService) {}

  ngOnInit() {
  }

  getUniqueItemsInShoppingCart(): Set<Item> {
    return this.shoppingCartService.getUniqueItemsInShoppingCart();
  }

  countItemsInShoppingCart(): number {
    return this.shoppingCartService.countItemsInShoppingCart();
  }
  orderItems() {
    return this.shoppingCartService.orderItems();
  }
}
