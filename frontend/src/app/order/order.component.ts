import { Component, OnInit } from '@angular/core';
import {ItemService} from 'src/app/services/item.service';
import {Item} from 'src/app/models/item';
import {Order} from 'src/app/models/order';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  orderedItems = [];
  constructor(private itemService: ItemService) { }



  ngOnInit() {
   this.getOrderedItems();
  }

  getOrderedItems() {
   this.itemService.getOrderedItems().subscribe((orders: Order[]) => {
      const uniqueIds = [...new Set(orders.map((order: Order) => order.itemId))];
      uniqueIds.forEach(id => {
          const count = orders.filter((order: Order) => order.itemId === id).length;
          this.itemService.getItem(id).subscribe(item => this.orderedItems.push(...Array(count).fill(item)));
      });
    });
  }
}
