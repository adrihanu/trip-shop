import { AppPage } from './app.po';
import { browser, logging } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

//  it('should load main page', () => {
//    page.navigateTo();
//    expect(page.getTitle()).toEqual('frontend');
//  });
//
//  it('should load login page', () => {
//    page.navigateTo('/signin');
//    expect(page.getButtonText()).toEqual('Login');
//  });

  it('should be able to login with correct password', () => {
   page.navigateTo('/signin');
   page.login("Okpass");
    //browser.getPageSource().then(function (res) {
      //console.log('Page source code is-' + res);
    //});
    expect(page.isLoggedIn()).toBe(true);
  });


  it('should not be able to login with incorrect password', () => {
   page.navigateTo('/signin');
   page.login("wrong_password");

   expect(browser.getCurrentUrl()).not.toEqual(browser.baseUrl);
   //expect(page.isLoggedIn()).toBe(false);
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);

  });
});
