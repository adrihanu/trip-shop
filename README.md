# Trip shop 
Simple web application for selling travel tours.  Written in `Angular` with `Firebase` as a backend (`BaaS`). I created this application as a university project during my master's studies.

Tags: `Angular`, `JavaScript`, `Firebase`, `Docker`, `Docker Compose`

## Running

1. Install `docker` and `docker-compose` on your system and make sure `dockerd` is running.
2. Run `docker-compose up`

## Testing

Unit/Integration:
Run `docker-compose exec frontend ng test` and visit `http://localhost:9876`

System:
Run: `docker-compose exec frontend ng e2e --webdriverUpdate=false --port 773`
Might be useful: `./node_modules/protractor/bin/webdriver-manager update --versions.chrome 79.0.3945.36`

## FireBase

Adjust `./src/environments/environment.ts` and `./src/environments/environment.prod.ts`.

### Collections

Create `items`, `usersComments`, `usersOrders`, `usersRoles` collections.

In `userRoles` add document with id `wDLjhey6J2ZH3HHIPkfcIhzCztz1` and set role to admin.

### Rules

```javascript
rules_version = '2';
service cloud.firestore {
  match /databases/{database}/documents {
  	match /items/{item} {
      allow read: if true;
      allow write: if request.auth.uid == 'wDLjhey6J2ZH3HHIPkfcIhzCztz1' ||
      (request.auth.uid != null && request.resource.data.limit > 0 && request.resource.data.price == null);
    }
    match /usersRoles/{usersRole} {
      allow read: if true;
      allow write: if false;
    }
  	match /usersOrders/{usersOrder} {
      allow read: if request.auth.uid != null;
      allow write: if request.auth.uid != null;
    }
    match /usersComments/{usersComment} {
      allow read: if true;
      allow write: if request.auth.uid != null;
    }
  }
}
```
